package uk.ac.tees.scm.u0012604.fragmentdemo.utilities;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class Reflection {

    /*
        enumerateImageViewChildren(final Class<? extends T> klass, final ViewGroup from)

        This performs a recursive search to find all the objects of type T (or subtypes) that
        are descendents of the ViewGroup that is passed into the method.

        It returns an ArrayList with all objects that are of Type T or derived from it.

        T is the allowable types

        klass
    */
    public static <T> ArrayList<T> enumerateImageViewChildren(final Class<? extends T> klass, final ViewGroup from) {

        final ArrayList<T> enumeratedImageViews = new ArrayList<>();

        for(int childViewIdx = 0; childViewIdx < from.getChildCount(); childViewIdx++) {

            final View childView = from.getChildAt(childViewIdx);

            // If the child is actually another ViewGroup, e.g. a Layout
            // then recursively call this method.
            //
            if(childView instanceof ViewGroup) {
                // Upon returning from the recursive call, we merge the results with this calls'
                // ArrayList
                //
                enumeratedImageViews.addAll((ArrayList<T>)enumerateImageViewChildren(klass, (ViewGroup)childView));
            }
            // Otherwise, check that the object is of the correct type and add to the collection if it is.
            //
            else {
                if(childView.getClass().isAssignableFrom(klass)) {
                    enumeratedImageViews.add((T)childView);
                }
            }
        }

        return enumeratedImageViews;
    }
}
