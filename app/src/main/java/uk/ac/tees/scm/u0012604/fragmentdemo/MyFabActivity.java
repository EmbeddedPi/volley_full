package uk.ac.tees.scm.u0012604.fragmentdemo;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MyFabActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    private static final String CHANNEL_ID = "My Channel";

    SenderFragment mSenderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fab);

        createSenderFragment();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        final Intent intent = getIntent();

        if(intent != null) {
            //Cancel the notification
            notificationManager.cancel(intent.getExtras().getInt("notificationId"));

            Toast.makeText(this, "You got here via a notification!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onReceiveMessage(String message) {
        final TextView tv = findViewById(R.id.specialMessage);

        tv.setText(message);
    }

    private void createSenderFragment() {
        // Create a SenderFragment dynamically
        final FragmentManager fm = getFragmentManager();

        final FragmentTransaction ft = fm.beginTransaction();

        ft.add(R.id.fabContainer, SenderFragment.newInstance());

        ft.commit();
    }

    public void sendNotification(View view) {
        final String channelName = getString(R.string.channel_name);

        final Notification notif = new Notification.Builder(this, CHANNEL_ID)
                                    .setContentTitle("This is a message")
                                    .setContentText("This is some content")
                                    .setSmallIcon(R.drawable.ic_my_notification_icon)
                                    .setChannelId(channelName)
                                    .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        notificationManager.notify(1, notif);
    }


}
