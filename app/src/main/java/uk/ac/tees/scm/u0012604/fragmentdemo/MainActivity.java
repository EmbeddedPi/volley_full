package uk.ac.tees.scm.u0012604.fragmentdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import uk.ac.tees.scm.u0012604.fragmentdemo.utilities.NotificationConfig;

public class MainActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String channelName = getString(R.string.channel_name);

        NotificationConfig.createNotificationChannel(channelName, this);
    }

    @Override
    public void onReceiveMessage(String message) {
        // Get the receiver fragment
        final ReceiverFragment fragment = (ReceiverFragment)getFragmentManager().findFragmentById(R.id.receiverFragment);

        if(fragment != null) {
            fragment.updateText(message);
        }
    }

    public void changeActivity(View view) {
        final Intent intent = new Intent(this, HorseActivity.class);
        startActivity(intent);
    }
}
